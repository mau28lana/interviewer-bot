import { createRouter, createWebHistory } from 'vue-router';
import DashboardPage from './screens/tabs/Dashboard.vue';
import ContactPage from './screens/tabs/ContactPage.vue';
import ContactDetail from './screens/detail-screen/ContactDetail.vue';
import BotResponse from './screens/components/BotResponse.vue'
import FormFix from './screens/tabs/FormPage(unused).vue';
import FormPage from './screens/tabs/Form.vue'
import FormDetail from './screens/detail-screen/FormDetail.vue';
import AddForm from "./screens/components/AddForm.vue";

const routes = [
  {
    path: '/',
    name: 'DashboardPage',
    component: DashboardPage
  },
  {
    path: '/formfix',
    name: 'FormFix',
    component: FormFix
  },
  {
    path: '/bot-response',
    name: 'BotResponse',
    component: BotResponse,
    // props: true
  },
  {
    path:'/add-form',
    name: "AddForm",
    component: AddForm
  },
  {
    path:'/form',
    name: "FormPage",
    component: FormPage
  },
  {
    path:'/form-detail/:id',
    name: 'FormDetail',
    component: FormDetail,
    props:true
  },
  {
    path:'/contact',
    name: 'ContactPage',
    component: ContactPage
  },
  {
    path:'/contact-detail/:id',
    name: 'ContactDetail',
    component: ContactDetail,
    props:true
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
